# pync

Simple folder synchronisation tool

---

## Project under development

Not recommended for use.

## Goal

to perform two way syncing between folders mounted on the same system.
i.e. `/home/michael/Documents` and `/media/michael/usbdrive/Documents`

## Usage

With absolute paths

```bash
./pync.py /path/to/primary /path/to/secondary
```

With relative paths

```bash
./pync.py primary/path secondary/path
```

## Contributions

Welcome.
