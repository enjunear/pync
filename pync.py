#!/usr/bin/python3
# pync - 2 way file sync
# Created by Michael Reeves 2020-01-16
# michael@enjunear.com

import argparse
import hashlib
import json
import os
import pathlib
import shutil
import sys
from distutils.util import strtobool

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


def createConfig(primary: str, secondary: str, type: str):
    # TODO
    os.mkdir(os.path.join(primary, ".pync"))
    return


def readConfig(folder: str):
    # TODO
    return


def readFileList(folder: str):
    # TODO
    with open(os.path.join(folder["folder"], ".pync"), "r") as filehandle:
        fl = json.load(filehandle)
    return fl


def findDeletes(folder: str):
    # TODO
    return


def printFolders(primary: str, secondary: str):
    if primary is not None and secondary is not None:
        print(primary, "<==>", secondary)
    else:
        print("Primary and secondary not defined.")
        sys.exit(1)
    return


def listFiles(folder: str):
    struct = list()
    for path, _dirs, files in os.walk(folder):
        for file in files:
            if file == ".pync":
                break
            if file.startswith(".") and not args.all:
                # skip hidden files, unless a -a flag is set.
                break
            else:
                filepath = os.path.join(path, file)
                relpath = os.path.relpath(filepath, folder)
                struct.append(
                    dict(
                        file=relpath,
                        path=filepath,
                        mtime=os.stat(filepath).st_mtime
                    )
                )
    return struct


def saveFileList(folder: str):
    fl = listFiles(folder)
    # don't need path item from each dict.
    for f in fl:
        f.pop("path")
    with open(os.path.join(folder, ".pync"), "w") as filehandle:
        json.dump(fl, filehandle)
    return


def md5sum(file):
    md5 = hashlib.md5()
    with open(file, "rb") as f:
        # Read and update hash in chunks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            md5.update(byte_block)
    return md5.hexdigest()


def compareSames(a: str, b: str):
    # pass in full dict item
    # compare the mtimes and md5sums
    # return the newer file, or null if identical
    # return dict in the form
    # {src="", dest=""}
    if md5sum(a["path"]) != md5sum(b["path"]):
        if a["mtime"] > b["mtime"]:
            return dict(
                src=a["path"],
                dest=b["path"]
            )
        elif b["mtime"] > a["mtime"]:
            return dict(
                src=b["path"],
                dest=a["path"]
            )
        else:
            print(
                "========================================",
                "Files differ, unable to determine newer file:",
                a["path"],
                b["path"],
                "Resolve this conflict manually and try again.",
                "========================================",
                sep="\n"
            )
    else:
        # md5 sums match - files are the same
        return


def diff(a: dict(), b: dict()):
    # read out the x["files"][n]["file"] to a set
    # using a set only gives the file name as dicts are not hashable.
    a_set = set([f["file"] for f in a["files"]])
    b_set = set([f["file"] for f in b["files"]])
    sames = a_set.intersection(b_set)
    a_only = a_set.difference(b_set)
    b_only = b_set.difference(a_set)
    print("\n========================================\n")
    print("Files in both folders")
    for s in sames:
        print(" " * 2, s)
    print("\n========================================\n")
    print("Files unique to", a["folder"])
    for i in a_only:
        print(" " * 2, i)
    print("\n========================================\n")
    print("Files unique to", b["folder"])
    for i in b_only:
        print(" " * 2, i)
    print("\n========================================\n")
    return a_only, sames, b_only


def prepCopies(srcFolder, files, destFolder):
    l = list()
    for file in files:
        l.append(
            dict(
                src=os.path.join(srcFolder, file),
                dest=os.path.join(destFolder, file)
            )
        )
    return l


def confirm(question: str, default="yes"):
    """
    "Borrowed" from http://code.activestate.com/recipes/577058/
    Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": "yes",   "y": "yes",  "ye": "yes",
             "no": "no",     "n": "no"}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def doCopies(files: list):
    for file in files:
        if file is not None:
            resp = confirm(
                "Copy file from " +
                file["src"] + " to " +
                file["dest"] + "?"
            )
            if strtobool(resp):
                # if necessary, make the relevent subfolders
                os.makedirs(os.path.dirname(file["dest"]), exist_ok=True)
                shutil.copy2(file["src"], file["dest"])
    return


def doDeletes():
    # TODO
    return


def main(primary, secondary, args):
    # common_path = os.path.commonpath([primary["path"], secondary["path"]])

    # TODO
    # load existing file lists
    # queue deletes
    doDeletes()

    primary["files"] = listFiles(primary["folder"])
    # print(primary["files"])
    secondary["files"] = listFiles(secondary["folder"])
    # print(secondary["files"])

    a_only, sames, b_only = diff(primary, secondary)
    # list of overwrite copies
    c = list()
    # ugly for nesting
    for item in sames:
        # find the corresponding item in primary
        for a in primary["files"]:
            if a["file"] == item:
                # find the corresponding item in secondary
                for b in secondary["files"]:
                    if b["file"] == item:
                        # send dicts off to compare
                        # the newer file comes back
                        # or None if the same
                        d = compareSames(a, b)
                        if d is not None:
                            c.append(d)
                        break
    # copy a_onlys to secondary
    c.extend(prepCopies(primary["folder"], a_only, secondary["folder"]))
    # copy b_onlys to primary
    c.extend(prepCopies(secondary["folder"], b_only, primary["folder"]))
    # do overwrite copies c
    doCopies(c)
    # save the current status of the folders after syncing.
    for folder in (primary, secondary):
        saveFileList(folder["folder"])
    sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Simple folder sync."
    )
    parser.add_argument(
        "primary",
        nargs="?",
        default=os.getcwd() + "/test/primary",
        help="/path/to/primary"
    )
    parser.add_argument(
        "secondary",
        nargs="?",
        default=os.getcwd() + "/test/secondary",
        help="/path/to/secondary"
    )
    parser.add_argument(
        "-i",
        "--ignore",
        nargs="*",
        help="ignore patterns"
    )
    # TODO: add option for no/limited recursion - maybe --depth=n
    parser.add_argument(
        "--depth",
        action="store",
        nargs=1,
        type=int,
        help="maximum depth for recursion")
    # TODO: add option to yes every prompt
    # TODO: add option to perform deletes first - handy for small usb drives
    # TODO: add -a option to include hidden files. still ignores .pync file
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="Include hidden files. (Ignores .pync file)."
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="Don't copy files."
    )
    parser.add_argument(
        "-v",
        dest="verbose",
        action="store_true"
    )
    args = parser.parse_args()

    primary = dict(
        path=os.path.abspath(args.primary),
        folder=args.primary
    )
    secondary = dict(
        path=os.path.abspath(args.secondary),
        folder=args.secondary,
    )

    main(primary, secondary, args)
