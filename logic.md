# Logic

## Goal

to perform two way syncing between folders mounted on the same system.

Ideally, to sync between my Documents folder and a folder on my usb drive, it would take a command like:

```bash
pync ~/Documents /media/michael/usbdrive/Documents
```

Or for a pre-configured folder:

```bash
pync /media/michael/usbdrive/Documents
```

which would read a config file at the specified directory.
That config file would then specify the paired directory to sync to.

---

The folders will be a primary - secondary type arrangement.
Naturally, the primary takes precedence.

---

On first run, create a .pync folder in the primary and secondary directories.
For each directory, walk the folder and list the structure into a JSON file.
Record file/folder metadata in the JSON file, particularly `mtime`.

---

1. On subsequent runs, read the structure of the directories into a new list.

1. Compare the old JSON file to the new list to identify deleted files.
    Store the paths of files to be deleted in an array.

1. Compare the modified time `mtime` of files.
    Store the path to the modified files in a dict with their modified time.

1. Compare the primary and secondary dicts to find conflicts.

1. For each conflict, prompt the user to pick the primary, secondary or skip.
    Delete the loser from the appropriate dict.

1. Perform copies.

1. Perform deletes.
    Optionally, perform deletes early on.

---

## JSON structure

```json
{
    config: {
        primary: "/path/to/primary",
        secondary: "/path/to/secondary",
        this: "primary"
    },
    folder: [
        {
            name: "1.txt",
            mtime: 12345678.123564
        },
        {
            name: "nested",
            // type: "folder",
            children: [
                {
                    name: "5.txt",
                    mtime: 9874163.5654
                },
                {...}
            ]
        },
        {...}
    ]
}
```

---

There are python packages that we can use to compare the directories.
By comparing the modified time, we can simply copy the newer file over the older file.
We can just use dircmp.

```python
from filecmp import dircmp
```

Deleting files is a problem.
When you delete a file on one device, any ordinary script would just copy the file back into place from the other device.
To remember if we deleted a file, we need to know what was in the folder before the sync.
i.e. we need to remember state.

Unfortunately, we can't know what was there before the first time we run pync.
But we can have pync take a note of what files are there, so we can delete them in the future.
We can use a simple sqlite3 database to keep a record.

```python
import sqlite3
```

Or alternatively, list the file tree to a JSON structure.

---

Now we need to check if this is the first run or not.

If it is the first run, then obviously, the database won't exist yet (or if the new flag is used).
If the database doesn't exist, we can just copy all the files from the left to the right.
We can use some switches to decide whether to:

- blindly overwrite the right,
- update with newer files only,
- delete files not on the left.

If the database exists, then this is not the first run and the databases should be compared to the folder contents.

1. First, check to see what has changed (including deletions).
2. If the same file has been changed on both sides, then update those files based on the flags (prompt, keep left, keep right, keep both).
3. Then, if deletions are allowed, propogate the deletions.
Alternatively, delete before updating to enable free space.
Maybe make this a flag option.

## TODO

- Figure out how the sqlite3 database is going to work.
- Figure out how to bulk copy/delete the files by passing the names around in python.

## Notes

```python
os.path.getmtime(file)
```

will return the modified time

There's probably a better way to do all this, but this is my go at it.
