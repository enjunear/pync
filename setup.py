#!/usr/bin/python3

from setuptools import setup
import distutils

setup(
    scripts=[
        "pync"
    ],
    entry_points={
        "console_scripts": [
            "pync = pync.py"
        ]
    }
)
